/*1*/
db.fruits.count({ onSale: true });

/*2*/
db.fruits.count({ stock: { $gte: 20 } });

/*3*/
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } }
]);

/*4*/
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } }
])

/*5*/
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } }
])